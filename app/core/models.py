from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, \
    PermissionsMixin
from django.utils import timezone
import uuid
from django.conf import settings
import os


def recommendation_image_file_path(instance, filename):
    """Generate new file path for new recommendation image"""
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'

    return os.path.join('uploads/recommendation/', filename)


class UserManager(BaseUserManager):

    def create_user(self, username, password=None, **extra_fields):
        """Create and save a user"""
        if not username:
            raise ValueError('Users must have a username')
        user = self.model(username=username,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password):
        """Creates and saves a new superuser"""
        user = self.create_user(username, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """User Model"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=225)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'


class Tag(models.Model):
    """Tag to be used for a recommendation"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Content(models.Model):
    """Content to be user for recommendation"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    other = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(
        null=True, upload_to=recommendation_image_file_path)

    def __str__(self):
        return self.name


class Recommendation(models.Model):
    """Recommendation table"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    title = models.CharField(max_length=255)
    link = models.CharField(max_length=255, blank=True)
    date_added = models.DateTimeField(
        default=timezone.now, blank=True)
    content = models.ManyToManyField('Content')
    tags = models.ManyToManyField('Tag')

    def __str__(self):
        return self.title


class Follow(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    recommendation = models.ForeignKey(
        Recommendation,  on_delete=models.CASCADE)

    def __str__(self):
        return self.recommendation.title
