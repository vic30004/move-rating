from unittest.mock import patch

from django.test import TestCase
from django.contrib.auth import get_user_model


from core import models


def sample_user(username='test12', password='testpass'):
    """Create a sample user"""
    return get_user_model().objects.create_user(username, password)


def sample_recommendation(user, **params):
    """Create and return a sample recipe"""
    defaults = {
        'title': 'Movies'
    }
    defaults.update(params)

    return models.Recommendation.objects.create(user=user, **defaults)


class ModelTests(TestCase):

    def test_create_user_with_username_successful(self):
        """Test creating a new user with an email and username is successful"""
        username = 'test12'
        password = 'testpass123'

        user = get_user_model().objects.create_user(
            username=username,
            password=password,
        )

        self.assertEqual(user.username, username)
        self.assertTrue(user.check_password(password))

    def test_new_user_invalid_email(self):
        """Test creating user with no email raises error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_a_superuser_is_created(self):
        """Test creating a new super user"""
        user = get_user_model().objects.create_superuser('test12',
                                                         'test12')
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_tag_str(self):
        """Test the tag string represantaion"""
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='Movies'
        )

        self.assertEqual(str(tag), tag.name)

    def test_content_str(self):
        """Test Creating a new content"""
        content = models.Content.objects.create(
            user=sample_user(),
            name='Test Movie',
        )
        self.assertEqual(str(content), content.name)

    def test_recommendation_str(self):
        """Test the recipe string representation"""
        recommendation = models.Recommendation.objects.create(
            user=sample_user(),
            title="Victor's recommended Movies",
        )
        self.assertEqual(str(recommendation), recommendation.title)

    @patch('uuid.uuid4')
    def test_recipe_file_name_uuid(self, mock_uuid):
        """Test that image is saved in the correct location"""
        uuid = 'test-uuid'
        mock_uuid.return_value = uuid
        file_path = models.recommendation_image_file_path(None, 'myimage.jpg')

        exp_path = f'uploads/recommendation/{uuid}.jpg'
        self.assertEqual(file_path, exp_path)

    def test_follow_str(self):
        """Test the folow string representation"""
        user = get_user_model().objects.create_user(
            username='test94',
            email='test23@gmail.com',
            password='password12',
        )
        follow = models.Follow.objects.create(
            user=user,
            recommendation=sample_recommendation(
                user=sample_user(username='test2'), title='Movies')
        )
        self.assertEqual(str(follow), follow.recommendation.title)
