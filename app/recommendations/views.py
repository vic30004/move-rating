from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from core.models import Tag, Content, Recommendation, Follow

from recommendations import serializers


class BaseRecommendationAttrViewSet(viewsets.GenericViewSet,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin):
    """Base viewset for user owned recommendation attr"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """Return objects for the authenticated user only"""
        assigned_only = bool(
            int(self.request.query_params.get('assigned_only', 0))
        )
        queryset = self.queryset
        if assigned_only:
            queryset = queryset.filter(recommendation__isnull=False)

        return queryset.filter(user=self.request.user).order_by(
            '-name').distinct()

    def perform_create(self, serializer):
        """Create new tag"""
        serializer.save(user=self.request.user)


class TagViewSet(BaseRecommendationAttrViewSet):
    """Manage Tags in the database"""
    queryset = Tag.objects.all()
    serializer_class = serializers.TagSerializer


class ContentViewSet(BaseRecommendationAttrViewSet):
    """Manage Contents in the database"""
    queryset = Content.objects.all()
    serializer_class = serializers.ContentSerializer

    def get_serializer_class(self):
        if self.action == 'upload-image':
            return serializers.ContentImageSerializer
        else:
            return serializers.ContentSerializer

    @action(methods=['POST'], detail=True, url_path='upload-image')
    def upload_image(self, request, pk=None):
        """Upload an image to a content"""
        content = self.get_object()
        serializer = serializers.ContentImageSerializer(
            content,
            data=request.data
        )
        if serializer.is_valid():
            print('working')
            serializer.save()
            print(serializer.data)
            return Response(serializer.data, status=status.HTTP_200_OK)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RecommendationViewSet(viewsets.ModelViewSet):
    """Manage Recommendations in the database"""
    serializer_class = serializers.RecommendationSerializer
    queryset = Recommendation.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def _params_to_array(self, qs):
        return [x for x in qs.split(',')]

    def get_queryset(self):
        """Retrieve the recommendations for the
           authenticated user"""
        tags = self.request.query_params.get('tags')
        content = self.request.query_params.get('content')
        queryset = self.queryset

        if tags:
            tag_ids = self._params_to_array(tags)
            queryset = queryset.filter(tags__id__in=tag_ids)

        if content:
            content_ids = self._params_to_array(content)
            queryset = queryset.filter(content__id__in=content_ids)

        return queryset.filter(user=self.request.user).order_by(
            '-date_added')

    def get_serializer_class(self):
        """Return appropriate serializer class"""
        if self.action == 'retrieve':
            return serializers.RecommendationDetailSerializer

        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new recommendation"""
        serializer.save(user=self.request.user)


class FollowViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,):
    """View set for the follow option"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.FollowSerializer
    queryset = Follow.objects.all()

    def get_queryset(self):
        """Return objects for the current authenticated user"""
        return self.queryset.filter(user=self.request.user)
