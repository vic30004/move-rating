from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Follow, Recommendation

from recommendations.serializers import FollowSerializer

FOLLOW_URL = reverse('recommendations:follow-list')


def sample_recommendation(user, **params):
    """Create and return a sample recommendation"""
    defaults = {
        'title': 'Movies'
    }
    defaults.update(params)

    return Recommendation.objects.create(user=user, **defaults)


def sample_follow(user, recommendation, **params):
    """Create and return a sample follow"""
    return Follow.objects.create(user=user, recommendation=recommendation)


class PublicRecommendationTests(TestCase):
    """Test unauthorized FOLLOW API access"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for retreiving tags"""
        res = self.client.get(FOLLOW_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateTagsApiTests(TestCase):
    """Test the authorized follow tag api"""

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            username='test@gmail.com',
            password='password123',
            email='test@gmail.com'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_follows(self):
        """Test retrieving follows"""
        user2 = get_user_model().objects.create_user(
            username='test323@gmail.com',
            password='password123',
            email='test323@gmail.com'
        )
        recommendation = sample_recommendation(user=user2)

        Follow.objects.create(user=self.user, recommendation=recommendation)
        # sample_follow(user2, recommendation)

        res = self.client.get(FOLLOW_URL)

        follow = Follow.objects.all()
        serializer = FollowSerializer(follow, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_folow_limited_to_user(self):
        """Test that follows returned are for the authenticated user"""
        user2 = get_user_model().objects.create_user(
            username='test324',
            password='testpass',
            email='test2@gmail.com'
        )
        recommendation = sample_recommendation(user=self.user)
        recommendation2 = sample_recommendation(user=user2)
        Follow.objects.create(user=user2, recommendation=recommendation)
        Follow.objects.create(user=self.user, recommendation=recommendation2)

        res = self.client.get(FOLLOW_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['title'], recommendation.title)
