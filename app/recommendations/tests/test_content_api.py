import tempfile
import os

from PIL import Image

from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Content, Recommendation

from recommendations.serializers import ContentSerializer

CONTENT_URL = reverse('recommendations:content-list')


def sample_content(user, name='James Bond'):
    """Create and return a sample content"""
    return Content.objects.create(user=user, name=name)


def image_upload_url(content_id):
    """Return url for content image upload"""
    return reverse('recommendations:content-upload-image', args=[content_id])


class PublicContentApiTests(TestCase):
    """Test the publicly available Content api"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for retrieving content"""
        res = self.client.get(CONTENT_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateContnetApiTests(TestCase):
    """Test the authorized user content API"""

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='test@gmail.com',
            password='password123',
            email='test@gmail.com'

        )
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_retrieve_content(self):
        """Test retrieving content"""
        Content.objects.create(user=self.user, name='James Bond')
        Content.objects.create(user=self.user, name='Harry Potter 7')

        res = self.client.get(CONTENT_URL)

        contents = Content.objects.all().order_by('-name')
        serializer = ContentSerializer(contents, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_content_limited_to_user(self):
        """Test that the content is returned for the authenticated user"""
        user2 = get_user_model().objects.create_user(
            username='test324',
            password='testpass',
            email='test2@gmail.com'
        )
        Content.objects.create(user=user2, name='Back to the future')
        contents = Content.objects.create(user=self.user, name='Dodgeball')

        res = self.client.get(CONTENT_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], contents.name)

    def test_create_content_successful(self):
        """Test creating a new content"""
        payload = {'name': 'test content'}
        self.client.post(CONTENT_URL, payload)

        exists = Content.objects.filter(
            user=self.user,
            name=payload['name']
        ).exists
        self.assertTrue(exists)

    def test_create_content_invalid(self):
        """Test creating a new tag with invalid payload"""
        payload = {'name': ''}
        res = self.client.post(CONTENT_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


class ContentImageUploadTests(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            username='test324',
            password='testpass',
            email='test2@gmail.com'
        )
        self.client.force_authenticate(self.user)
        self.content = sample_content(user=self.user)

    def tearDown(self):
        self.content.image.delete()

    def test_upload_image_to_content(self):
        """Test uploading an image to content"""
        url = image_upload_url(self.content.id)
        with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:
            img = Image.new('RGB', (10, 10))
            img.save(ntf, format='JPEG')
            ntf.seek(0)
            res = self.client.post(url, {'image': ntf}, format='multipart')

            self.content.refresh_from_db()
            self.assertEqual(res.status_code, status.HTTP_200_OK)
            self.assertIn('image', res.data)
            self.assertTrue(os.path.exists(self.content.image.path))

    def test_upload_image_bad_request(self):
        """Test uploading an invalid image"""
        url = image_upload_url(self.content.id)
        res = self.client.post(url, {'image': 'notimage'}, format='multipart')

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_content_assigned_to_recommendation(self):
        """Test retrieving content by thoise assigned to recommendation"""
        content1 = Content.objects.create(user=self.user, name='James Bond')
        content2 = Content.objects.create(
            user=self.user, name='Harry Potter 7')

        recommendation = Recommendation.objects.create(
            title='Movies',
            user=self.user
        )

        recommendation.content.add(content1)

        res = self.client.get(CONTENT_URL, {'assigned_only': 1})

        serializer1 = ContentSerializer(content1)
        serializer2 = ContentSerializer(content2)
        self.assertIn(serializer1.data, res.data)
        self.assertNotIn(serializer2.data, res.data)

    def test_retrieving_content_assigned_unqique(self):
        """Test filterin content by assigned returns unqiue items"""
        content = Content.objects.create(user=self.user, name='James Bond')
        Content.objects.create(
            user=self.user, name='Harry Potter 7')

        recommendation1 = Recommendation.objects.create(
            title='Movies',
            user=self.user
        )
        recommendation1.content.add(content)
        recommendation2 = Recommendation.objects.create(
            title='Tv Show',
            user=self.user
        )
        recommendation2.content.add(content)

        res = self.client.get(CONTENT_URL, {'assigned_only': 1})
        self.assertEqual(len(res.data), 1)
