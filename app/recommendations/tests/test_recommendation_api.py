from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Recommendation, Tag, Content

from recommendations.serializers import RecommendationSerializer, \
    RecommendationDetailSerializer


RECOMMENDATION_URL = reverse('recommendations:recommendation-list')


def detail_url(recommendation_id):
    """Return recommendation detail url"""
    return reverse('recommendations:recommendation-detail',
                   args=[recommendation_id])


def sample_tag(user, name='Movie'):
    """create and return a sample tag"""
    return Tag.objects.create(user=user, name=name)


def sample_content(user, name='James Bond'):
    """Create and return a sample content"""
    return Content.objects.create(user=user, name=name)


def sample_recommendation(user, **params):
    """Create and return a sample recipe"""
    defaults = {
        'title': 'Movies'
    }
    defaults.update(params)

    return Recommendation.objects.create(user=user, **defaults)


class PublicRecommendationTests(TestCase):
    """Test unauthorized recommendation API access"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for retreiving tags"""
        res = self.client.get(RECOMMENDATION_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateTagsApiTests(TestCase):
    """Test the authorized user tags API"""

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            username='test@gmail.com',
            password='password123',
            email='test@gmail.com'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_recommendation(self):
        """Test retrieving recommendation"""
        sample_recommendation(user=self.user)
        sample_recommendation(user=self.user)

        res = self.client.get(RECOMMENDATION_URL)

        recommendation = Recommendation.objects.all().order_by('-date_added')
        serializer = RecommendationSerializer(recommendation, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_recommendation_limited_to_user(self):
        """Test that recommendation returned are for authenticated user"""
        user2 = get_user_model().objects.create_user(
            username='test324',
            password='testpass',
            email='test2@gmail.com'
        )
        sample_recommendation(user=user2)
        sample_recommendation(user=self.user)

        res = self.client.get(RECOMMENDATION_URL)

        recommendations = Recommendation.objects.filter(user=self.user)
        serializer = RecommendationSerializer(recommendations, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data, serializer.data)

    def test_view_recommendation_detail(self):
        """Test viewing a recommendation detail"""
        recommendation = sample_recommendation(user=self.user)
        recommendation.tags.add(sample_tag(user=self.user))
        recommendation.content.add(sample_content(user=self.user))

        url = detail_url(recommendation.id)
        res = self.client.get(url)

        serializer = RecommendationDetailSerializer(recommendation)
        self.assertEqual(res.data, serializer.data)

    def test_create_basic_recommendation(self):
        """Test creating Recommendation"""
        payload = {
            'title': 'My Movies',
        }
        res = self.client.post(RECOMMENDATION_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        recommendation = Recommendation.objects.get(id=res.data['id'])
        for key in payload.keys():
            self.assertEqual(payload[key], getattr(recommendation, key))

    def test_create_recommendation_with_tags(self):
        """Test creating recommendations with tags"""
        tag1 = sample_tag(user=self.user, name='Comedy')
        tag2 = sample_tag(user=self.user, name='Italian')

        payload = {
            'title': 'My Comedy Movies',
            'tags': [tag1.id, tag2.id]
        }

        res = self.client.post(RECOMMENDATION_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        recommendation = Recommendation.objects.get(id=res.data['id'])
        tags = recommendation.tags.all()
        self.assertEqual(tags.count(), 2)
        self.assertIn(tag1, tags)
        self.assertIn(tag2, tags)

    def test_create_recommendation_with_content(self):
        """Test creating recommendations with content"""
        content1 = sample_content(user=self.user, name='James Bond')
        content2 = sample_content(user=self.user, name='Harry Potter')
        payload = {
            'title': 'My Fav Movies',
            'content': [content1.id, content2.id]
        }
        res = self.client.post(RECOMMENDATION_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        recommendation = Recommendation.objects.get(id=res.data['id'])
        content = recommendation.content.all()
        self.assertEqual(content.count(), 2)
        self.assertIn(content1, content)
        self.assertIn(content2, content)

    def test_partial_update_recommendation(self):
        """Test updating recommendation using PATCH"""
        recommendation = sample_recommendation(user=self.user)
        recommendation.tags.add(sample_tag(user=self.user))
        new_tag = sample_tag(user=self.user, name='Video Games')

        payload = {'title': 'Movies', 'tags': [new_tag.id]}
        url = detail_url(recommendation.id)
        self.client.patch(url, payload)

        recommendation.refresh_from_db()
        self.assertEqual(recommendation.title, payload['title'])
        tags = recommendation.tags.all()
        self.assertEqual(len(tags), 1)
        self.assertIn(new_tag, tags)

    def test_full_update_recommendation(self):
        """Test updating recommendation with PUT"""
        recommendation = sample_recommendation(user=self.user)
        recommendation.tags.add(sample_tag(user=self.user))
        payload = {'title': 'Games'}
        url = detail_url(recommendation.id)
        self.client.patch(url, payload)

        recommendation.refresh_from_db()
        self.assertEqual(recommendation.title, payload['title'])
        tags = recommendation.tags.all()
        self.assertEqual(len(tags), 1)

    def test_filter_recommendations_by_tags(self):
        """Test returning recommendation with specific tag"""
        recommendation1 = sample_recommendation(user=self.user, title='Movies')
        recommendation2 = sample_recommendation(
            user=self.user, title='Video Games')

        tag1 = sample_tag(user=self.user, name='Action')
        tag2 = sample_tag(user=self.user, name='RPG')

        recommendation1.tags.add(tag1)
        recommendation2.tags.add(tag2)

        recommendation3 = sample_recommendation(user=self.user, title='Food')

        res = self.client.get(
            RECOMMENDATION_URL,
            {'tags': f'{tag1.id},{tag2.id}'}
        )

        serializer1 = RecommendationSerializer(recommendation1)
        serializer2 = RecommendationSerializer(recommendation2)
        serializer3 = RecommendationSerializer(recommendation3)
        self.assertIn(serializer1.data, res.data)
        self.assertIn(serializer2.data, res.data)
        self.assertNotIn(serializer3.data, res.data)

    def test_filter_recommendations_by_content(self):
        """Test returing recommendations with specific content"""
        recommendation1 = sample_recommendation(user=self.user, title='Movies')
        recommendation2 = sample_recommendation(
            user=self.user, title='Video Games')

        content1 = sample_content(user=self.user, name='James Bond')
        content2 = sample_content(user=self.user, name='Dragon Hunter')

        recommendation1.content.add(content1)
        recommendation2.content.add(content2)
        recommendation3 = sample_recommendation(user=self.user, title='Food')

        res = self.client.get(
            RECOMMENDATION_URL,
            {'content': f'{content1.id},{content2.id}'}
        )

        serializer1 = RecommendationSerializer(recommendation1)
        serializer2 = RecommendationSerializer(recommendation2)
        serializer3 = RecommendationSerializer(recommendation3)
        self.assertIn(serializer1.data, res.data)
        self.assertIn(serializer2.data, res.data)
        self.assertNotIn(serializer3.data, res.data)
