from rest_framework import serializers

from core.models import Tag, Content, Recommendation, Follow


class TagSerializer(serializers.ModelSerializer):
    """Serializer for tag objects"""

    class Meta:
        model = Tag
        fields = ('id', 'name')
        read_only_fields = ('id',)


class ContentSerializer(serializers.ModelSerializer):
    """Serializer for content Objects"""

    class Meta:
        model = Content
        fields = ('id', 'name', 'other', 'image')
        read_only_fields = ('id',)


class RecommendationSerializer(serializers.ModelSerializer):
    """Serializer for recommendation Objects"""
    content = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Content.objects.all()
    )
    tags = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Tag.objects.all()
    )

    class Meta:
        model = Recommendation
        fields = ('id', 'title', 'link', 'content', 'tags',
                  'date_added')
        read_only_fields = ('id',)


class RecommendationDetailSerializer(RecommendationSerializer):
    """Serialize a recipe detail"""
    content = ContentSerializer(many=True, read_only=True)
    tags = TagSerializer(many=True, read_only=True)


class ContentImageSerializer(serializers.ModelSerializer):
    """Serializer for uploading image to content"""

    class Meta:
        model = Content
        fields = ('id', 'image')
        read_only_fields = ('id', )


class FollowSerializer(serializers.ModelSerializer):
    """Serializer for the follow option"""
    title = serializers.CharField(
        source='recommendation.title',
        read_only=True
    )
    recommendation_id = serializers.CharField(
        source='recommendation.id', read_only=True)

    class Meta:
        model = Follow
        fields = ('id', 'title', 'recommendation_id')
        read_only_fields = ('id', )
