from django.urls import path, include
from rest_framework.routers import DefaultRouter

from recommendations import views

router = DefaultRouter()
router.register('tags', views.TagViewSet)
router.register('content', views.ContentViewSet)
router.register('recommendation', views.RecommendationViewSet)
router.register('follow', views.FollowViewSet)

app_name = 'recommendations'

urlpatterns = [
    path('', include(router.urls))
]
