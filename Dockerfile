FROM nikolaik/python-nodejs:python3.8-nodejs14-alpine



LABEL Victor AA

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt

RUN pip install --upgrade pip
RUN apk add nano
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev musl-dev zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

COPY ./package.json /package.json 
COPY ./webpack.config.js /webpack.config.js
COPY ./.babelrc /.babelrc
RUN npm install

RUN mkdir /app
WORKDIR /app
COPY ./app /app



RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user


WORKDIR ./..
USER root
RUN npm run dev

WORKDIR /app
